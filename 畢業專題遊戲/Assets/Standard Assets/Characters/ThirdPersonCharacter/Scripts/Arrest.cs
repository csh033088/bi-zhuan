﻿using System;
using UnityEngine;
using System.Collections;
using Photon.Pun.Demo.PunBasics;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(AICharacter))]
    public class Arrest : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
        public AICharacter character { get; private set; } // the character we are controlling
        public Transform target; // target to aim for
        public float width = 10; //Your square width
        float rx;
        float rz;
        float count;


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<AICharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;

            count = 3;
        }

        private void Update()
        {
            SetTarget(GameObject.FindWithTag("Prison").transform);


            if (target != null && (Vector3.Distance(transform.position, target.position) <= 100))
                agent.SetDestination(target.position);


            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);

        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }


        public bool Countdown()
        {
            count -= Time.deltaTime;
            if (count > 0)
                return false;
            else
            {
                count = 2;
                return true;
            }
        }
    }
}