using System;
using UnityEngine;
using System.Collections;
using Photon.Pun;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(AICharacter))]
    public class AICharacterControl : MonoBehaviourPunCallbacks,IPunObservable
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public AICharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        public float width = 10; //Your square width
        float rx;
        float rz;
        float count;



        private void Start()
        {
            

            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<AICharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;

            count = 3;
        }


        private void Update()
        {
            try
            {
                SetTarget(GameObject.FindWithTag("victims").transform);//�i�H�g�}�C
            }
            catch (NullReferenceException e)
            {
                SearchAround();
            }


            if (target != null && (Vector3.Distance(transform.position, target.position) <= 100))
                agent.SetDestination(target.position);

            else
            {

                SearchAround();
            }

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);


        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        public void SearchAround()
        {
            if (Countdown())
            {
                rx = UnityEngine.Random.Range(-1, 2);
                rz = UnityEngine.Random.Range(-1, 2);
                //Debug.Log(rx+","+rz);
                agent.SetDestination(new Vector3(transform.position.x + (rx * width), Terrain.activeTerrain.SampleHeight(transform.position), transform.position.z + (rz * width)));
                Vector3 euler = transform.eulerAngles;
                euler.y = UnityEngine.Random.Range(0f, 360f);
                transform.eulerAngles = euler;
            }
        }

        public bool Countdown()
        {
            count -= Time.deltaTime;
            if (count > 0)
                return false;
            else
            {
                count = 2;
                return true;
            }
        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            //throw new NotImplementedException();
        }
    }
}