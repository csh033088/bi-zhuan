﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(AICharacter))]
    public class PoliceControl : MonoBehaviour
    {
        void Start()
        {
            GetComponent<Arrest>().enabled = false;
        }
        // Start is called before the first frame update
        void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == "victims")
            {
                GetComponent<AICharacterControl>().enabled = false;
                GetComponent<Arrest>().enabled = true;

            }
        }
    }
}