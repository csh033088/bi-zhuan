﻿using System;
using UnityEngine;
using System.Collections;
using Photon.Pun;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(AICharacter))]
    public class AIDog : MonoBehaviourPunCallbacks, IPunObservable
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public AICharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        public float width = 10; //Your square width
        float rx;
        float rz;
        

        // public float WSpeed ; //更改人物移速
        // GameObject dogGameObject;
        GameObject witch;
        bool wakeup = false;




        private void Start()
        {

            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<AICharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;

            

            // dogGameObject = GameObject.FindGameObjectWithTag("witch");
           


        }


        private void Update()
        {

            if (wakeup)
            {
                
                try
                {
                    SetTarget(GameObject.FindWithTag("witch").transform);
                }
                catch (NullReferenceException e)
                {
                    return;
                }


                agent.SetDestination(target.position);


                if (agent.remainingDistance > agent.stoppingDistance)
                    character.Move(agent.desiredVelocity, false, false);

                // WSpeed = dogGameObject.GetComponent<ThirdPersonCharacter>().m_MoveSpeedMultiplier;
            }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
        void OnCollisionEnter(Collision a) //自訂碰撞自身a   小狗程式
        {
            if (a.gameObject.CompareTag("witch"))
            {
                //witch.GetComponent<ThirdPersonCharacter>().m_MoveSpeedMultiplier = 0f;
                Destroy(gameObject, 5);
            }
            if (a.gameObject.tag == "victims")
            {
                wakeup = true;
            }
        }
        void OnDestroy()
        {
           // witch.GetComponent<ThirdPersonCharacter>().m_MoveSpeedMultiplier = 1f;
        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            //throw new NotImplementedException();
        }
    }
}