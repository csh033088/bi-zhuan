﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using TMPro;
using Photon.Pun;

public class PlayerListItem : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    [SerializeField] private TMP_Text Text;
    //public Player players { get; private set; }
    public Player players;

    public void SetUp(Player _players)
    {
       players = _players;
       //Text.text = PhotonNetwork.CurrentRoom.Name;
       Text.text = _players.NickName;
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (players == otherPlayer)
        {
            Destroy(gameObject);
        }
    }

    public override void OnLeftRoom()
    {
        Destroy(gameObject);
    }
}
