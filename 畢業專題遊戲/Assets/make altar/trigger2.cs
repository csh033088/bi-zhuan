﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class trigger2 : MonoBehaviour
{
    public Slider slider;
    public GameObject slider1;
    public Text text;
    [SerializeField] float Speed = 0.1f;

    private void Update()
    {
        if (Gamedata.sv2 == 1)
            Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "witch")
        {
            slider1.SetActive(true);
            slider.value = Gamedata.sv2;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //   if (Input.GetKey(KeyCode.F))
        if (other.gameObject.tag == "witch")
        {
            {
                slider.value += (Time.deltaTime * Speed);
                text.text = (int)(slider.value * 100) + "%";
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "witch")
        {
            Gamedata.sv2 = slider.value;
            slider1.SetActive(false);
        }
    }
}
