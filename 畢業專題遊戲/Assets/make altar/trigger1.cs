﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class trigger1 : MonoBehaviour
{
     public Slider slider;
     public GameObject slider1;
     public Text text;
    [SerializeField] float Speed = 0.1f;

    private void Update()
    {
        if (Gamedata.sv1 == 1)
            Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
     {
        if (other.gameObject.tag == "witch")
        {
            slider1.SetActive(true);
            slider.value = Gamedata.sv1;
        }
     }
     private void OnTriggerStay(Collider other)
     {
          if (other.gameObject.tag == "witch")
        {
           //if (Input.GetKey(KeyCode.F))
            {
                slider.value += (Time.deltaTime * Speed);
                text.text = (int)(slider.value * 100) + "%";
            }
        }
    }
     private void OnTriggerExit(Collider other)
     {
        if (other.gameObject.tag == "witch")
        {
            Gamedata.sv1 = slider.value;
            slider1.SetActive(false);
        }
     }
}