﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadeMedicine : MonoBehaviour
{
    public Transform righthand;
    public float speed = 2f;
    void Update()
    {
        if (Vector3.Distance(righthand.position, transform.position) > 0.1f)
        {
            PetSmothFlow();
        }
    }
    void PetSmothFlow()
    {
        transform.position = Vector3.Lerp(righthand.position, righthand.position, Time.deltaTime * speed);
    }
}