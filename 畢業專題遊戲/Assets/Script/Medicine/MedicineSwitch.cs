﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicineSwitch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<MadeMedicine>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Gamedata.church == 1)// 教堂完成
        {
            GetComponent<MadeMedicine>().enabled = true;
        }
    }
}
