﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour
{
    Transform TargetTrans;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TargetTrans = GameObject.Find("EthanBody").transform;
        if (Vector3.Distance(transform.position, TargetTrans.position)<=100) {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(TargetTrans.position.x, TargetTrans.position.y+10, TargetTrans.position.z), 0.6f);
            transform.rotation = Quaternion.Slerp(transform.rotation, TargetTrans.rotation, 0.8f);
        }//else(moving around)
        
    }
}
