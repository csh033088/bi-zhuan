﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setThirdPOS : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject thirdperson;
    float count = 1;

    void Start()
    {
        Countdown();
        thirdperson = GameObject.Find("ThirdPersonController");
        //thirdperson.transform.position = new Vector3(162,10, 122);
        //Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        //cam.enabled = true;
    }

    // Update is called once per frame
    public bool Countdown()
    {
        count += Time.deltaTime;
        if (count > 0)
            return false;
        else
        {
            count = 10;
            return true;
        }
    }
}
