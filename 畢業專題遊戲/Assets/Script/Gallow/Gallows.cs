﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.CompilerServices;

public class Gallows : MonoBehaviour
{
    public Slider slider;
    public GameObject slider1;
    public Text text;
    [SerializeField] public float Speed = 0.1f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "victims")
        {
            slider1.SetActive(true);
            slider.value = Gamedata.Gallows;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        // if (Input.GetKey(KeyCode.F))
        {
            if (other.gameObject.tag == "victims")
            {
                slider.value += (Time.deltaTime * Speed);
                text.text = (int)(slider.value * 100) + "%";

                Gamedata.Gallows = slider.value;

                if (Gamedata.Gallows == 1)
                {
                    Destroy(other.gameObject);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "victims")
        {
            Gamedata.Gallows = slider.value;
            slider1.SetActive(false);
        }
    }
}