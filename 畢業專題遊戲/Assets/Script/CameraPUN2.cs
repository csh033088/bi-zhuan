﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun.Demo.PunBasics;
using Photon.Pun;

public class CameraPUN2 : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {
		////////////////////////Photon Camera
		CameraWork _cameraWork = this.gameObject.GetComponent<CameraWork>();
		if (_cameraWork != null)
		{
			if (photonView.IsMine)
			{
				_cameraWork.OnStartFollowing();
			}
		}
		else
		{
			Debug.LogError("playerPrefab- CameraWork component 遺失",
				this);
		}
		/////////////////////////Photon Camera 
	}

	// Update is called once per frame
}
