﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraOff : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.enabled = false;

    }
}