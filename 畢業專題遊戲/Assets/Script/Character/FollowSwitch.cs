﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
public class FollowSwitch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Followcontrol>().enabled = false;
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Police")
        {
            GetComponent<Followcontrol>().enabled = true;
        }
    }

}