﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Followcontrol : MonoBehaviour
{
    public Transform police;//警官
    public float speed = 1f;//移速，值越小，移動越平缓
    // Use this for initialization
    // Update is called once per frame

    void Update()
    {//如果警官和嫌疑犯的距離大於1，控制嫌疑犯跟警官移動
        police = GameObject.FindWithTag("Police").transform;
        if (Vector3.Distance(police.position, transform.position) > 1f)
        {
            PetSmothFlow();
        }
        //控制嫌疑犯朝向警官
        transform.LookAt(police.position);
    }
    //控制嫌疑犯的位置平滑移動
    void PetSmothFlow()
    {
        transform.position = Vector3.Lerp(police.position, police.position, Time.deltaTime * speed);
    }



}