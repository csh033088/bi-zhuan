﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rescue : MonoBehaviour//警察消失、跟隨模式關閉
{
    public GameObject Police;
    public GameObject Victims;

    void OnTriggerEnter(Collider a) //自訂碰撞自身a
    {
        if (a.tag == "Police")
        {
            Destroy(this.Police);
        }

        if (a.tag == "victims")
        {
            Victims.GetComponent<Followcontrol>().enabled = false;
        }

    }

}