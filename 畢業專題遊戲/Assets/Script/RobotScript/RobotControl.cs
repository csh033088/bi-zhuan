﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;

namespace Robot
{
    public class RobotControl : MonoBehaviourPunCallbacks
    {
        private Animator animator;

        [SerializeField]
        private float directionDampTime = 0.25f;
        [SerializeField] public bool speed =true;


        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            if (!animator)
            {
                Debug.LogError(
                    "RobotControl- Animator component 遺失", this);
            }
            /////////////////////////////////////////////

        }

        // Update is called once per frame
        void Update()
        {
            ///玩家角色本人時, 才會往下執行
            if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
            {
                return;
            }
            ///移動
            if (!animator)
            {
                return;
            }
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            if (v < 0)
            {
                v = 0;
            }
            if(speed)
            animator.SetFloat("Speed", h * h + v * v);
            //////轉彎，!!!!!!(注意)directionDampTime 的數值愈大, My Robot Kyle 跑動的轉彎半徑也愈大.
            animator.SetFloat("Direction", h, directionDampTime,
            Time.deltaTime);
            ///跑跳
            AnimatorStateInfo stateInfo =
            animator.GetCurrentAnimatorStateInfo(0);

            // 只有在跑動時, 才可以跳躍.
            if (stateInfo.IsName("Base Layer.Run"))
            {
                if (Input.GetButtonDown("Jump"))
                {
                    animator.SetTrigger("Jump");
                }
            }
            //////////////////////////////////////////
        }
    }
}
