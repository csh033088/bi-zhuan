﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using System.Linq;
using PhotonHashTable = ExitGames.Client.Photon.Hashtable;
public class LaunchT1 : MonoBehaviourPunCallbacks//,IPunObservable
{
    public static LaunchT1 Instance;

    [SerializeField] TMP_InputField roomNameInputField;
    [SerializeField] TMP_Text errorText;
    [SerializeField] TMP_Text roomNameText;
    [SerializeField] Transform roomListContent;
    [SerializeField] GameObject roomListItemPrefab;
    [SerializeField] TMP_InputField playerNameInputField;
    [SerializeField] Transform playerListContent;
    [SerializeField] GameObject PlayerListItemPrefab;
    [SerializeField] GameObject startGameButton;

    private PhotonHashTable customProperties = new PhotonHashTable();


    [SerializeField] TMP_Text RegionText;

    static int count = 0;
    
    void Awake()
    {

        Instance = this;

    }

    // Start is called before the first frame update
    void Start()
    {
        if (!PhotonNetwork.NickName.Equals(""))
        {
            playerNameInputField.text = PhotonNetwork.NickName;
            ConnectToLobby();
        }
    }
    private void Update()
    {
        //Debug.Log(victims + "  " + witch);
    }
    public void ConnectToLobby()
    {
        Debug.Log("Connectting to Lobby");
        if (!PhotonNetwork.IsConnected)
            //PhotonNetwork.ConnectToRegion("asia");
            PhotonNetwork.ConnectUsingSettings();
        else
        {
            if (Gamedata.InRoom)
            {
                Gamedata.InRoom = false;
                PhotonNetwork.LeaveRoom();
                PhotonNetwork.JoinLobby();
                MenuManager.Instance.OpenMenu("Room");
            }
            else
            {
                PhotonNetwork.JoinLobby();
                Debug.Log("Connected - - -");
                MenuManager.Instance.OpenMenu("Loading");
            }
            LeaveRoom();

        }
        PhotonNetwork.NickName = playerNameInputField.text;

    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");
        PhotonNetwork.JoinLobby();
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("JoinedLobbyCallBack");
        if (!Gamedata.InFindRoom)
        {
            //RegionText.text = PhotonNetwork.CloudRegion;
            //RegionText.text = PhotonNetwork.CloudRegion;
            MenuManager.Instance.OpenMenu("Title");
            Debug.Log("Joined lobby");
        }
        else
        {
            MenuManager.Instance.OpenMenu("Find Room");
            Gamedata.InFindRoom = true;
        }
    }
    public void CreateRoom()//title button
    {
        if (string.IsNullOrEmpty(roomNameInputField.text))
        {
            return;
        }
        PhotonNetwork.CreateRoom(roomNameInputField.text);
        Gamedata.InFindRoom = true;
        MenuManager.Instance.OpenMenu("Loading");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.CustomProperties["Teams"].ToString());
        if (PhotonNetwork.CurrentRoom.Name.Equals("empty"))
        {
            return;
        }
        Gamedata.InRoom = true;
        MenuManager.Instance.OpenMenu("Room");
        roomNameText.text = PhotonNetwork.CurrentRoom.Name;
        Player[] players = PhotonNetwork.PlayerList;

        foreach (Transform child in playerListContent)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < players.Count(); i++)
        {
            Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(players[i]);
        }
        startGameButton.SetActive(PhotonNetwork.IsMasterClient);
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        startGameButton.SetActive(PhotonNetwork.IsMasterClient);
    }

    public void JoinRoom(RoomInfo info)
    {
        if (info.PlayerCount < 4)
        {
            PhotonNetwork.JoinRoom(info.Name);
            MenuManager.Instance.OpenMenu("Loading");
        }
    }
    public override void OnCreatedRoom()
    {
        if (PhotonNetwork.CurrentRoom.Name.Equals("empty"))
        {
            PhotonNetwork.LeaveRoom();
        }
        base.OnCreatedRoom();
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = "Room Creation Failed: " + message;
        MenuManager.Instance.OpenMenu("Error");
    }

    public void StartGame()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount < 1)//控制進入人數
        {
            print("Player not enough! Now only " + PhotonNetwork.CurrentRoom.PlayerCount);
        }
        else
        {
            PhotonNetwork.LoadLevel("map");
            //Debug.Log(witch+" "+victims);
        }
    }
    public void Ready()
    {
         //not implement;
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
        count = 0;
        Gamedata.InRoom = false;
        MenuManager.Instance.OpenMenu("Find Room");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log("OnRoomListUpdate");
        count++;
        if (count == 2)
        {
            count = 0;
            return;
        }
        foreach (Transform trans in roomListContent)
        {
            Destroy(trans.gameObject);
        }

        Debug.Log("List count = " + roomList.Count);
        Debug.Log("Photon room count = " + PhotonNetwork.CountOfRooms);

        for (int i = 0; i < roomList.Count; i++)
        {
            Debug.Log(roomList[i].Name + " esist");
            if (roomList[i].RemovedFromList)
            {
                Debug.Log(roomList[i].Name + " close");
                continue;
            }
            Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().SetUp(roomList[i]);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(newPlayer);
    }

    public void LeaveLobby()
    {
        PhotonNetwork.LeaveLobby();
    }
    public override void OnLeftLobby()
    {
        Debug.Log("Left lobby");
        Gamedata.InFindRoom = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene("main menu");
        PhotonNetwork.Disconnect();
    }
    public void UpdateRoomList()
    {
        Gamedata.InFindRoom = true;
        PhotonNetwork.CreateRoom("empty");
    }
    public void WitchPlayer()
    {
        customProperties.Add("Teams", "witch");
        PhotonNetwork.SetPlayerCustomProperties(customProperties);
        Debug.Log(PhotonNetwork.LocalPlayer.CustomProperties["Teams"].ToString());
        ConnectToLobby();
    }
    public void VictimPlayer()
    {
        customProperties.Add("Teams", "victims");
        PhotonNetwork.SetPlayerCustomProperties(customProperties);
        Debug.Log(PhotonNetwork.LocalPlayer.CustomProperties["Teams"].ToString());
        ConnectToLobby();
    }

    /*public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(witch);
            stream.SendNext(victims);
        }
        else
        {
            this.witch = (bool)stream.ReceiveNext();
            this.victims = (bool)stream.ReceiveNext();
        }
    }*
    /*void OnPhotonPlayerDisconnected()
{
   Debug.Log("Disconnect");
}*/
}
    