﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class church : MonoBehaviour
{
    public Slider slider;
    public GameObject slider1;
    public Text text;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "victims" && Gamedata.ChurchUnLock == 1) {
            slider1.SetActive(true);
            slider.value = Gamedata.church;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //   if (Input.GetKey(KeyCode.F))
        if (other.gameObject.tag == "victims" && Gamedata.ChurchUnLock == 1)
        {
         
            slider.value += (Time.deltaTime * 0.5f);
            text.text = (int)(slider.value * 20) + "/20";
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "victims" && Gamedata.ChurchUnLock == 1)
        {
            Gamedata.church = slider.value;
            slider1.SetActive(false);
        }
    }
}
