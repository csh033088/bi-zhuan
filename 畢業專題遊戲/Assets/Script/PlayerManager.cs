﻿using UnityEngine;
using Photon.Pun.Demo.PunBasics;
using Photon.Pun;
using Photon.Realtime;

namespace Robot
{
    public class PlayerManager : MonoBehaviourPunCallbacks, 
        IPunObservable
    {
        [SerializeField] Transform police;
        [SerializeField] Transform witch;
        [SerializeField] float speed = 1f;
        [SerializeField] GameObject Myself;
        // Start is called before the first frame update
        private bool BePoliceCatch= false;
        [SerializeField] public Rigidbody rb;
        void Start()
        {   
            ////相機
            CameraWork _cameraWork =
            this.gameObject.GetComponent<CameraWork>();

            if (_cameraWork != null)
            {
                if (photonView.IsMine)
                {
                    _cameraWork.OnStartFollowing();
                }
            }
            else
            {
                Debug.LogError("playerPrefab- CameraWork component 遺失",
                    this);
            }
            rb = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            police  =   GameObject.FindWithTag("Police").transform;
            witch   =   GameObject.FindWithTag("witch").transform;
            if (BePoliceCatch)
            {//跟隨警察位置
                //Debug.Log("ddd");
                Follow(police);
            }
            if (this.gameObject.CompareTag("victims") && Vector3.Distance(witch.position, transform.position) < 0.1f)//當靠近女巫時 關閉isKinematic 才可以觸發條件
            {
                rb.isKinematic = false;
                Destroy(this.gameObject,0.5f);
            }
        }
        public void OnPhotonSerializeView(PhotonStream stream,
        PhotonMessageInfo info)////////////(重要)連線時互相更新玩家資料
        {
            //Trans = this.transform;
            /* if (stream.IsWriting)
             {
                 // 為玩家本人的狀態, 將 IsFiring 的狀態更新給其他玩家
                 //stream.SendNext(IsFiring);
                 Debug.Log("為玩家本人的狀態, 將 IsFiring 的狀態更新給其他玩家");
             }
             else
             {
                 // 非為玩家本人的狀態, 單純接收更新的資料
                 //this.IsFiring = (bool)stream.ReceiveNext();
                 Debug.Log("非為玩家本人的狀態, 單純接收更新的資料");
             }*/
        }
         void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Police")&& this.gameObject.CompareTag("victims"))
            {
                GetComponent<CharacterController>().enabled = false;
                GetComponent<RobotControl>().speed = false;
                Invoke("DogCatch", 5);
                //我盡力了QQ

                Debug.Log("被警察觸摸");
                BePoliceCatch = true;
                Debug.Log(this.tag);
            }
            if (other.gameObject.CompareTag("Dog") && this.gameObject.CompareTag("witch"))
            {
                Debug.Log("被小狗觸摸");
                this.gameObject.GetComponent<RobotControl>().speed = false;
                Invoke("DogCatch",6);
            }
            
            if (other.gameObject.CompareTag("victims")  && this.gameObject.CompareTag("witch") && Gamedata.Potion == 1)
            {
                Debug.Log("女巫被嫌疑人觸摸");
                Destroy(this.gameObject);
                Gamedata.victims = 0;
            }
            //    如果玩家為女巫                                         且         被嫌疑人碰撞                   且    手上有藥水 
            /*if (this.gameObject.CompareTag("witch") && other.gameObject.CompareTag("victims") && Gamedata.Potion == 1)
            {
                Debug.Log("嫌疑人被女巫觸摸");
                Destroy(this.gameObject);

            }*/
        }
        void DogCatch()
        {
            this.gameObject.GetComponent<RobotControl>().speed = true;
        }
        public void Follow(Transform target)
        {
            if (Vector3.Distance(target.position, transform.position) > 0.1f)
            {
                
                transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * speed);
            }
            transform.LookAt(target.position);
        }
        public void SetTarget(Transform target)
        {
            this.police = target;
            this.witch = target;
        }
    }
}
