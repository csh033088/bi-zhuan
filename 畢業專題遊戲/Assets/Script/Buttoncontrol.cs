﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Buttoncontrol : MonoBehaviour
{
    // Start is called before the first frame update
    // Update is called once per frame

    /*public GameObject CMB;
    public GameObject MC;
    public void CameraTurnback()
    {
        MC.SetActive(false);
        CMB.SetActive(true);
       //GameObject.Find("Main Camera").transform.Rotate(0,180,0);
    }
    public void CameraTurn()
    {
        MC.SetActive(true);
        CMB.SetActive(false);
        //GameObject.Find("Main Camera").transform.Rotate(0,180,0);
    }*/
    public void Witch()
    {
        StartCoroutine(CWitch());
        GameObject.Find("ThirdPersonController").tag = "witch";
        DontDestroyOnLoad(GameObject.Find("ThirdPersonController"));
        
    }
    public void Victims()
    {
        StartCoroutine(CWitch());
        GameObject.Find("ThirdPersonController").tag = "victims";
        DontDestroyOnLoad(GameObject.Find("ThirdPersonController"));
        
    }
    public void GoBack()
    {
        SceneManager.LoadScene("main menu");
    }
    public void Set()
    {
        SceneManager.LoadScene("set");
    }
    public void Story()
    {
        SceneManager.LoadScene("Story");
    }
    public void Menu()
    {
        SceneManager.LoadScene("main menu");
    }
    public void ChoosePlayer()
    {
        SceneManager.LoadScene("choose player");
    }
    public void ConnectLobby()
    {
        SceneManager.LoadScene("Lobby");
    }
    IEnumerator CWitch()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("map");
        while (!asyncLoad.isDone)
        {

            yield return null;
        }
    }
}
