﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using PhotonHashTable = ExitGames.Client.Photon.Hashtable;

public class MainScript : MonoBehaviourPunCallbacks
{

    public static MainScript Instance;

    [SerializeField] GameObject [] altar;
    public GameObject text;
    bool count = true;

    //創建角色
    public GameObject playerPrefab;
    public GameObject AIPolice;
    private PhotonView PI;

    //PhotonNetwork.LocalPlayer.CustomProperties("Teams")
    void Awake()
    {

        Instance = this;

    }
    void Start()
    {
        PI = GetComponent<PhotonView>();
        //創建角色
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player.CustomProperties["Teams"].ToString().Equals("witch")&&player.IsLocal)//選擇女巫賦予TAG並且生成角色
            {
                //Debug.Log(PhotonNetwork.LocalPlayer.CustomProperties["Teams"].ToString());
                this.playerPrefab.tag = "witch";
                CreatePalyers();
                Debug.Log(player.NickName + " is" + this.playerPrefab.tag);
            }
            if (player.CustomProperties["Teams"].ToString().Equals("victims") && player.IsLocal)//選擇嫌疑人賦予TAG並且生成角色
            {
                //Debug.Log(PhotonNetwork.LocalPlayer.CustomProperties["Teams"].ToString());
                this.playerPrefab.tag = "victims";
                CreatePalyers();
                Debug.Log(player.NickName+" is"+this.playerPrefab.tag);
            }
        }
        for (int i = 0; i < altar.Length; i++)//祭壇顯現
        {
            altar[i].SetActive(true);
        }
        /*if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.InstantiateRoomObject("AIPolice",new Vector3(166f, 10f,130f),Quaternion.identity, 0, null) ;
            //CreateAI();
        }*/
        /*Instantiate(altar,new Vector3(Random.Range(-180.0f, 414.0f), 0f, Random.Range(-678.0f, -135.0f)), altar5.transform.rotation);/*  int Random_PointsX = Random.Range(346,824);
        int Random_PointsZ = Random.Range(218,751);*/
    }


    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space)) {
        if (Gamedata.church == 1)//藥水製作完成
        {
            text.SetActive(true);
            Gamedata.Potion = 1;
            //text.GetComponent<UnityEngine.UI.Text>().text = "Potion\n  1 Glass";
        }
        if(Gamedata.sv1 == 1 && Gamedata.sv2 == 1 && Gamedata.sv3 == 1&& Gamedata.sv4 == 1&& Gamedata.sv5 == 1 )//當五個祭壇完成時 嫌疑犯失敗  
            MenuManager.Instance.OpenMenu("VictimsLose");
                   
        //if (Gamedata.victims == 0)
            //MenuManager.Instance.OpenMenu("VictimsWin");
        /*if (Gamedata.church == 1)
        {
            Potion.SetActive(true);
        }*/
    }
    void OnGUI()
    {
        if (Gamedata.ClueRc == 3 && count)//3個線索集齊，教堂解鎖
        {
            GUI.Label(new Rect(20, 20, 100, 100), "Church is unlocked");
            Gamedata.ChurchUnLock = 1;
            Invoke("Countdown", 5f);//5秒後消失提醒字串
        }
        if (Gamedata.church == 1)
        {
            GUI.Label(new Rect(20, 60, 200, 200), "Potion 1 Glass");
        }
    }
    void Countdown()
    {
        count = false;
    }
    public void CreatePalyers()
    {
            Debug.LogFormat("動態生成玩家角色 {0}", this.playerPrefab.name);           
                PhotonNetwork.Instantiate(this.playerPrefab.name,
                    new Vector3(Random.Range(166.0f, 180.0f), 10f, Random.Range(136.0f, 160.0f)), Quaternion.identity, 0);   //生成位置改亂數
    }
    public void CreateAI()
    {  
            Debug.LogFormat("動態生成玩家角色 {0}", Application.loadedLevelName);
            PhotonNetwork.InstantiateRoomObject(AIPolice.name,
                new Vector3(Random.Range(166.0f, 180.0f), 10f, Random.Range(136.0f, 160.0f)), Quaternion.identity);   //生成位置改亂數
    }
    public void LeaveLobby()
    {
        Gamedata.InFindRoom = false;
        SceneManager.LoadScene("main menu");
        MenuManager.Instance.OpenMenu("MainUI");
        Gamedata.sv1 = 0;
        Gamedata.sv2 = 0;
        Gamedata.sv3 = 0;
        Gamedata.sv4 = 0;
        Gamedata.sv5 = 0;
        Gamedata.ChurchUnLock = 0;
        Gamedata.church = 0;
        //Gamedata.witch = 0;
        //Gamedata.victims = 0;
        Gamedata.Potion = 0;
        Gamedata.ClueRc = 0;
        Gamedata.Gallows = 0;
       // PhotonNetwork.Disconnect();
    }
    public void TextUse()
    {
        Gamedata.sv1 = 1; Gamedata.sv2 = 1; Gamedata.sv3 =1; Gamedata.sv4 = 1; Gamedata.sv5 = 1;
    }
    public void TextUse2()
    {
        Gamedata.Potion = 1;
    }


}
public class Gamedata :ScriptableObject
{
        public static float sv1 = 0;
        public static float sv2 = 0;
        public static float sv3 = 0;
        public static float sv4 = 0;
        public static float sv5 = 0;
        public static float ChurchUnLock = 0;
        public static float church = 0;
        public static float Potion = 0;
        public static float ClueRc = 0;
        public static float witch = 0;
        public static float victims = 0;
        public static float Gallows = 0;
        public static bool InRoom = false;
        public static bool InFindRoom = false;

}



