﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class clue : MonoBehaviour
{
    public GameObject Player;
    public GameObject Cube;
    void OnTriggerEnter(Collider a) //自訂碰撞自身a
    {
        if (a.tag == "victims")
        {               
            Gamedata.ClueRc+=1;
            Destroy(this.Cube);           
        }
    }
}